Build instructions

Packages required:

SciDAC QMP  https://github.com/usqcd-software/qmp
SciDAC QIO  https://github.com/usqcd-software/qio
SciDAC LIME submodule https://github.com/usqcd-software/c-lime.git

Clone the SciDAC QMP and QIO packages.
Clone the LIME package under other_libs/c-lime
Build and install the SciDAC packages (configure, make)
Modify the Makefiles under ks_spectrum and ks_imp_rhmc to reflect the locations of the SciDAC packages
Unpack the MILC tarball

In directory ks_spectrum, make ks_spectrum_hisq.  This is the executable for the fpi benchmark.
In the directory ks_imp_rhmc, make su3_rhmd_hisq.  This is the executable for the hmc benchmark.



