# Baseline benchmark problem: fpi component

## Description

- Brief description:  Decay constant analysis.  Lattice size 144^3 x 288
- Code used: MILC ks_spectrum_hisq MILC code version 7.7.10 double precision
- Edison compute nodes: 1024
- Metric: Time to calculate a prescribed set of propagators and hadron correlators for one source time
- Status: Existing run dated June 2016.  The Edison executable is probably still available.  All inputs are available.
- Propose for Perlmutter: Rerun the benchmark with Grid/GPU or QUDA HISQ inverters replacing the MILC inverters.
- Required inputs:
   *  fpi_input.baseline
   *  A suitable 144^3 x 288 lattice.  See /project/projectdirs/m1759/www/lqcd/milc-benchmark-lattice/l144288f211b700m000569m01555m1827a.1062

Note: The fpi_input.baseline file will need editing for the machine node geometry and the lattice file name.

## Figure of Merit

Time per complete analysis, scaled to full machine.

# Measurement on Edison

The Edison output is provided in fpi_output.baseline.  The problem was
run on 1024 Edison compute nodes. The output file line

   Machine = QMP (portable), with 49152 nodes

uses "nodes" to mean MPI ranks.  With 2 ranks per core and 24 cores
per node the number of nodes is 49152/48 = 1024.  Edison has 5586
nodes, according to
https://www.nersc.gov/users/computational-systems/edison/configuration/
, which represents a fraction 0.18 of the machine.

The output file line

   Time = 2.621664e+04 seconds

gives to total time for the calculation.  So the baseline figure of
merit, which gives the time per scientific result, scaled to the full
machine, is the time to completion multiplied by the fraction of the
machine, or

   FOM = 26217*0.18 = 4806 machine-seconds.
