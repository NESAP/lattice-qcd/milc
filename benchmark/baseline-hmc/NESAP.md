# Baseline benchmark problem: HMC component

## Description

- Brief description:  HISQ lattice generation.  Lattice size 144^3 x 288
- Code used: MILC/SciDAC QOPQDP su3_rhmd_hisq MILC code version 7.7.10
- Edison compute nodes: 768
- Metric: Time to do run one HMC trajectory of length 2
- Status:  Existing run dated December 2016.  The Edison executable is probably still available.  All inputs are available.
- Propose for Perlmutter:  Rerun the benchmark with Grid/GPU or QUDA modules replacing the QOPQDP modules.
- Required inputs:
   *  hmc_input.baseline
   *  rat.m000569m01555m1827_u95
   *  A suitable 144^3 x 288 lattice.  See /project/projectdirs/m1759/www/lqcd/milc-benchmark-lattice/l144288f211b700m000569m01555m1827a.1062


Note: The hmc_input.baseline file will need editing for the machine node geometry and the lattice file name.

## Figure of Merit

Time per single trajectory of length two molecular-dynamics time
units, scaled to the full machine.

# Measurement on Edison

The Edison output is provided in hmc_output.baseline.  The problem was
run on 768 Edison compute nodes.The output file line

   Machine = QMP (portable), with 36864 nodes

uses "nodes" to mean MPI ranks.  There were two ranks per core, so 48
ranks per node. So the number of nodes is 36864/48 = 768.  Edison has
5586 nodes, according to
https://www.nersc.gov/users/computational-systems/edison/configuration/
, which represents a fraction 0.14 of the machine.

The output file line

   Time = 2.526890e+04 seconds

gives to total time for the calculation.  So the baseline figure of
merit, which gives the time per scientific result, scaled to the full
machine, is the time to completion multiplied by the fraction of the
machine, or

   FOM = 25269*0.14 = 3474 machine-seconds.
